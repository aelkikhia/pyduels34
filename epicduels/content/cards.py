from epicduels.content.game_state import Card as Cd

# Combat Cards
COMBAT_5_1 = {'type': Cd.combat, 'name': 'combat', 'attack': 5, 'defense': 1,
              'owner': ''}
COMBAT_4_2 = {'type': Cd.combat, 'name': 'combat', 'attack': 4, 'defense': 2,
              'owner': ''}
COMBAT_4_1 = {'type': Cd.combat, 'name': 'combat', 'attack': 4, 'defense': 1,
              'owner': ''}
COMBAT_3_3 = {'type': Cd.combat, 'name': 'combat', 'attack': 3, 'defense': 3,
              'owner': ''}
COMBAT_3_2 = {'type': Cd.combat, 'name': 'combat', 'attack': 3, 'defense': 2,
              'owner': ''}
COMBAT_3_1 = {'type': Cd.combat, 'name': 'combat', 'attack': 3, 'defense': 1,
              'owner': ''}
COMBAT_2_4 = {'type': Cd.combat, 'name': 'combat', 'attack': 2, 'defense': 4,
              'owner': ''}
COMBAT_2_3 = {'type': Cd.combat, 'name': 'combat', 'attack': 2, 'defense': 3,
              'owner': ''}
COMBAT_2_2 = {'type': Cd.combat, 'name': 'combat', 'attack': 2, 'defense': 2,
              'owner': ''}
COMBAT_2_1 = {'type': Cd.combat, 'name': 'combat', 'attack': 2, 'defense': 1,
              'owner': ''}
COMBAT_1_5 = {'type': Cd.combat, 'name': 'combat', 'attack': 1, 'defense': 5,
              'owner': ''}
COMBAT_1_4 = {'type': Cd.combat, 'name': 'combat', 'attack': 1, 'defense': 4,
              'owner': ''}
COMBAT_1_2 = {'type': Cd.combat, 'name': 'combat', 'attack': 1, 'defense': 2,
              'owner': ''}

# Special Cards
CALM = {'name': 'Calm', 'type': Cd.special, 'owner': '',
        'description': 'Move Anakin up to 8 spaces. If, after playing this '
                       'card you have no cards in your hand, draw up to 5 '
                       'cards.'}
CHILDREN_OF_THE_FORCE = {'name': 'Children Of The Force', 'type': Cd.special,
                         'owner': '',
                         'description': 'Move Luke up to 6 spaces. Then move '
                                        'Leia up to 6 spaces. Draw 2 cards'}
CHOKE = {'name': 'Choke', 'type': Cd.special, 'owner': '',
         'description': 'Choose any minor character. That character receives '
                        '6 damage.'}
FLAME_THROWER = {'name': 'Flame Thrower', 'type': Cd.special, 'owner': '',
                 'description': 'Flame Thrower does 2 damage to all characters'
                                ' adjacent to Jango Fett. You may then move '
                                'these characters up to 3 spaces each.'}
FIRE_UP_THE_JET_PACK = {'name': 'Fire Up The Jet Pack', 'type': Cd.special,
                        'owner': '',
                        'description': 'You may move Jango Fett to any empty '
                                       'space. Playing this card does not '
                                       'count as an action.'}
FORCE_BALANCE = {'name': 'Force Balance', 'type': Cd.special, 'owner': '',
                 'description': 'All players discard their hands. Each player '
                                'draws 3 new cards.'}
FORCE_DRAIN = {'name': 'Force Drain', 'type': Cd.special, 'owner': '',
               'description': 'Pick 2 cards at random from an opponent\'s '
                              'hand. That player must discard those cards.'}
FORCE_LIFT = {'name': 'Force Lift', 'type': Cd.special, 'owner': '',
              'description': 'Turn any character adjacent to Yoda on its side.'
                             ' This character cannot move, attack, or defend. '
                             'At any time, any player may discard 3 cards to '
                             'stand this character up.'}
FORCE_LIGHTNING = {'name': 'Force Lightning', 'type': Cd.special,
                   'owner': '',
                   'description': 'Choose any character. That character '
                                  'receives 3 damage. The player "controlling '
                                  'this character must discard a card at '
                                  'random.'}
FORCE_PUSH_DOOKU = {'name': 'Force Push', 'type': Cd.special, 'owner': '',
                    'description': 'Move any character adjacent to to any '
                                   'empty space. That character receives 1 '
                                   'damage.'}
FORCE_PUSH_YODA = {'name': 'Force Push', 'type': Cd.special, 'owner': '',
                   'description': 'Move any character adjacent to to any '
                                  'empty space. That character receives 3 '
                                  'damage.'}
FORCE_QUICKNESS = {'name': 'Force Quickness', 'type': Cd.special, 'owner': '',
                   'description': 'Move Obi-Wan up to 8 spaces, then draw a '
                                  'card'}
FUTURE_FORESEEN = {'name': 'Future Foreseen', 'type': Cd.special,
                   'owner': '',
                   'description': 'Look at the top 4 cards of your draw pile. '
                                  'Put one in your hand and put the other 3 '
                                  'cards back on top of your draw pile in any '
                                  'order.'}
GAIN_POWER = {'name': 'Gain Power', 'type': Cd.special, 'owner': '',
              'description': 'Draw 3 cards'}
GIVE_ORDERS = {'name': 'Give Orders', 'type': Cd.special, 'owner': '',
               'description': 'Move Dooku up to 4 spaces. Then move Super '
                              'Battledroid 1 up to 4 spaces and move Super '
                              'Battledroid 2 up to 4 spaces.'}
I_WILL_NOT_FIGHT_YOU = {'name': 'I Will Not Fight You', 'type': Cd.special,
                        'owner': '',
                        'description': 'Choose an opponent. You and the chosen'
                                       ' opponent reveal your hands. Both of '
                                       'you discard all cards with an attack '
                                       'value greater than 1.'}
INSIGHT = {'name': 'Insight', 'type': Cd.special, 'owner': '',
           'description': 'Look at any opponent\'s hand. Then choose one '
                          'card. Your opponent must discard the chosen card.'}
ITS_NOT_WISE = {'name': 'Its Not Wise', 'type': Cd.special, 'owner': '',
                'description': 'Move any character adjacent to Chewbacca up to'
                               ' 3 spaces. That character receives 3 damage.'}
JEDI_MIND_TRICK = {'name': 'Jedi Mind Trick', 'type': Cd.special, 'owner': '',
                   'description': 'Take any card from your discard pile and '
                                  'put that card in your hand.'}
LET_GO_OF_YOUR_HATRED = {'name': 'Let Go Of Your Hatred', 'type': Cd.special,
                         'owner': '',
                         'description': 'Choose an opponent. That opponent '
                                        'chooses and discards 2 cards.'}
LUKES_IN_TROUBLE = {'name': 'Luke\'s In Trouble', 'type': Cd.special,
                    'owner': '',
                    'description': 'If Leia is adjacent to Luke, Luke recovers'
                                   ' 3 damage. If Luke has been destroyed, '
                                   'Leia recovers 3 damage.'}
MEDITATION = {'name': 'Meditation', 'type': Cd.special, 'owner': '',
              'description': 'The Emperor recovers up to 4 damage. Choose an '
                             'opponent. That opponent cannot draw cards during'
                             ' his/her next turn.'}
NEVER_TELL_ME_THE_ODDS = {'name': 'Never Tell Me The Odds', 'type': Cd.special,
                          'owner': '',
                          'description': 'Han does 2 damage to all opponents '
                                         'characters Han can attack. Then you '
                                         'may shuffle your discard pile into '
                                         'your draw pile.'}
PROTECTION = {'name': 'Protection', 'type': Cd.special, 'owner': '',
              'description': 'If Anakin is alive, Padme recovers 4 damage. If '
                             'Anakin has been destroyed, Padme recovers 2 '
                             'damage.'}
ROYAL_COMMAND = {'name': 'Royal Command', 'type': Cd.special, 'owner': '',
                 'description': 'Exchange spaces between the Emperor and any '
                                'Crimson Guard.'}
THERMAL_DETONATOR = {'name': 'Thermal Detonator', 'type': Cd.special,
                     'owner': '',
                     'description': 'Thermal Detonator does 4 damage to any '
                                    'one character Boba Fett can attack. All '
                                    'character adjacent to that character also'
                                    ' receive 4 damage.'}
THROW_DEBRIS = {'name': 'Throw Debris', 'type': Cd.special, 'owner': '',
                'description': 'Choose any character. That character receives '
                               '4 damage.'}
WHIRLWIND = {'name': 'Whirlwind', 'type': Cd.special, 'owner': '',
             'description': 'Mace does 4 damage to all opponents characters '
             'he can attack.'}
WISDOM = {'name': 'Wisdom', 'type': Cd.special, 'owner': '',
          'description': 'You may move Mace up to 5 Spaces, then draw a card.'}
WOOKIE_HEALING = {'name': 'Wookie Healing', 'type': Cd.special, 'owner': '',
                  'description': 'Chewbacca recovers up to 3 damage. The you '
                                 'may move Chewbacca up to 5 spaces.'}
WOOKIE_INSTINCTS = {'name': 'Wookie Instincts', 'type': Cd.special, 'owner': '',
                    'description': 'Search your draw pile for the Bowcaster '
                                   'Attack card. If it is in your draw pile, '
                                   'put it in your hand. Then shuffle your '
                                   'draw pile.'}
WRATH_ANAKIN = {'name': 'Wrath Anakin', 'type': Cd.special, 'owner': '',
                'description': 'You may move Anakin adjacent to any minor '
                               'Character. That character receives 7 damage.'}
WRATH_VADER = {'name': 'Wrath Vader', 'type': Cd.special, 'owner': '',
               'description': 'Choose an opponent. All of that opponent\'s '
                              'characters receive 2 damage.'}
WRIST_CABLE = {'name': 'Wrist Cable', 'type': Cd.special, 'owner': '',
               'description': 'Wrist Cable does 2 damage to any one character '
                              'Fett can attack. The player controlling the '
                              'attacked character gets 1 less action on '
                              'his/her next turn'}
YOU_WILL_DIE = {'name': 'You Will Die', 'type': Cd.special, 'owner': '',
                'description': 'Choose an opponent. That opponent must discard'
                               ' his/her entire hand.'}
YOUR_SKILLS_ARE_NOT_COMPLETE = {'name': 'Your Skills Are Not Complete',
                                'type': Cd.special, 'owner': '',
                                'description': 'Choose any opponent. That '
                                               'opponent must reveal his/her '
                                               'hand and discard all special '
                                               'cards.'}
SUDDEN_ARRIVAL = {'name': 'Sudden Arrival', 'type': Cd.special, 'owner': '',
                  'description': 'Move Greedo adjacent to any character. '
                                 'Playing this card does not count as an '
                                 'action.'}

# Special Combat Cards
BATTLEMIND = {'name': 'Battlemind', 'type': Cd.power_combat, 'owner': '',
              'description': 'The attack and defense values of this card are '
                             'equal to the number of cards in your hand after '
                             'this card is played.'}
LATENT_FORCE_ABILITY = {'name': 'Latent Force Abilities',
                        'type': Cd.power_combat, 'attack': 7, 'defense': 7,
                        'owner': '',
                        'description': 'Draw 1 card'}

# Special Attack Cards
ANGER = {'name': 'Anger', 'type': Cd.power_attack, 'attack': 8,
         'owner': '', 'description': 'After attacking, discard every card in '
                                     'your hand, except one card.'}
ALL_TO_EASY = {'name': 'All To Easy', 'type': Cd.power_attack, 'attack': 3,
               'owner': '',
               'description': 'If this card is not blocked, the attacked '
                              'character receives 20 damage instead of 3.'}
ASSASSINATION = {'name': 'Assassination', 'type': Cd.power_attack, 'attack': 7,
                 'owner': '', 'description': 'After attacking, you may move '
                                             'Zam to an empty space.'}
ATHLETIC_SURGE = {'name': 'Athletic Surge', 'type': Cd.power_attack,
                  'attack': 8, 'owner': '',
                  'description': 'After attacking, you may move Darth Maul up '
                                 'to 6 spaces.'}
BOWCASTER = {'name': 'Bowcaster', 'type': Cd.power_attack, 'attack': 11,
             'owner': '',
             'description': 'Search your draw pile for the Bowcaster Attack '
                            'card. If it is in your draw pile, put it in your '
                            'hand. Then shuffle your draw pile.'}
DARK_SIDE_DRAIN = {'name': 'Dark Side Drain', 'type': Cd.power_attack,
                   'attack': 3, 'owner': '',
                   'description': 'If Vader does damage to a character with '
                                  'this card, Vader recovers the amount of '
                                  'damage done to the character.'}
DEADLY_AIM = {'name': 'Deadly Aim', 'type': Cd.power_attack, 'attack': 7,
              'owner': '', 'description': 'Draw 2 cards'}
DESPERATE_SHOT = {'name': 'Desperate Shot', 'type': Cd.power_attack,
                  'attack': 7, 'owner': '',
                  'description': 'If you do not destroy the defending '
                                 'character with this card, destroy Greedo.'}
FORCE_CONTROL = {'name': 'Force Control', 'type': Cd.power_attack, 'attack': 7,
                 'owner': '', 'description': 'After attacking, you may move '
                                             'all characters in play up to 3 '
                                             'spaces each.'}
FORCE_STRIKE = {'name': 'Force Strike', 'type': Cd.power_attack, 'attack': 6,
                'owner': '', 'description': 'Draw 1 card'}
GAMBLERS_LUCK = {'name': 'Gambler\'s Luck', 'type': Cd.power_attack,
                 'attack': 4, 'owner': '',
                 'description': 'If Han does damage with this card, choose an '
                                'opponent to discard a card at random.'}
HEROIC_RETREAT = {'name': 'Heroic Retreat', 'type': Cd.power_attack,
                  'attack': 5, 'owner': '',
                  'description': 'After attacking, you may move Han up to 5 '
                                 'spaces.'}
JEDI_ATTACK = {'name': 'Jedi Attack', 'type': Cd.power_attack, 'attack': 6,
               'owner': '', 'description': 'After attacking, you may move '
                                           'Obi-wan up to 6 spaces.'}
JUSTICE = {'name': 'Justice', 'type': Cd.power_attack, 'attack': 4,
           'owner': '', 'description': 'If Leia has been destroyed, the attack'
                                       ' value of this card is 10.'}
KYBER_DART = {'name': 'Kyber Dart', 'type': Cd.power_attack, 'attack': 9,
              'owner': '',
              'description': '"After attacking, if you destroy the defending '
                             'character, draw 3 cards'}
MASTERFUL_FIGHTING = {'name': 'Masterful Fighting', 'type': Cd.power_attack,
                      'attack': 5, 'owner': '', 'description': 'Draw 1 card'}
MISSILE_LAUNCH = {'name': 'Missile Launch', 'type': Cd.power_attack,
                  'attack': 7, 'owner': '', 'description': 'Draw 4 cards'}
PRECISE_SHOT = {'name': 'Precise Shot', 'type': Cd.power_attack, 'attack': 9,
                'owner': '', 'description': 'After attacking, you may discard '
                                            'a card to draw a card.'}
TAUNTING = {'name': 'Taunting', 'type': Cd.power_attack, 'attack': 7,
            'owner': '', 'description': 'Draw 1 card'}
ROCKET_RETREAT = {'name': 'Rocket Retreat', 'type': Cd.power_attack,
                  'attack': 4, 'owner': '',
                  'description': '"After attacking, you may move Fett to an '
                                 'empty space.'}
SHOT_ON_THE_RUN = {'name': 'Shot On The Run', 'type': Cd.power_attack,
                   'attack': 6, 'owner': '',
                   'description': 'After attacking, you may move Padme up to 6'
                                  ' spaces.'}
SITH_SPEED = {'name': 'Sith Speed', 'type': Cd.power_attack, 'attack': 3,
              'owner': '', 'description': 'Playing this card does not count as'
                                          ' an action.'}
SNIPER_SHOT = {'name': 'Sniper Shot', 'type': Cd.power_attack, 'attack': 3,
               'owner': '', 'description': 'If this card is not blocked, Zam '
                                           'does 6 damage instead of 3.'}
SUPER_SITH_SPEED = {'name': 'Super Sith Speed', 'type': Cd.power_attack,
                    'attack': 4, 'owner': '',
                    'description': 'Playing this card does not count as an '
                                   'action.'}

# Special Defense Cards
BLINDING_SURGE = {'name': 'Blinding Surge', 'type': Cd.power_defense,
                  'defense': 0, 'owner': '',
                  'description': 'After taking the attacker\'s damage, Darth '
                                 'Maul does 3 points of damage to the '
                                 'attacking character.'}
COUNTER_ATTACK = {'name': 'Counterattack', 'type': Cd.power_defense,
                  'defense': 0, 'owner': '',
                  'description': 'Anakin receives no damage from the attack. '
                                 'Instead the attacker receives 1 damage.'}
FORCE_REBOUND = {'name': 'Force Rebound', 'type': Cd.power_defense,
                 'defense': 0, 'owner': '',
                 'description': 'Yoda receives no damage from the attack. '
                                'Instead, the attacker receives damage equal '
                                'to the attack number on the attacker\'s '
                                'card.'}

MARTIAL_DEFENSE = {'name': 'Martial Defense', 'type': Cd.power_defense,
                   'defense': 10, 'owner': '', 'description': 'Draw 1 card'}

SERENITY = {'name': 'Serenity', 'type': Cd.power_defense, 'defense': 15,
            'owner': '', 'description': 'Draw 1 card'}

JEDI_BLOCK = {'name': 'Jedi Block', 'type': Cd.power_defense, 'defense': 12,
              'owner': '', 'description': 'Draw 1 card'}

all_cards = [CALM,
             CHILDREN_OF_THE_FORCE,
             CHOKE,
             FLAME_THROWER,
             FIRE_UP_THE_JET_PACK,
             FORCE_BALANCE,
             FORCE_DRAIN,
             FORCE_LIFT,
             FORCE_LIGHTNING,
             FORCE_PUSH_DOOKU,
             FORCE_PUSH_YODA,
             FORCE_QUICKNESS,
             FUTURE_FORESEEN,
             GAIN_POWER,
             GIVE_ORDERS,
             I_WILL_NOT_FIGHT_YOU,
             INSIGHT,
             ITS_NOT_WISE,
             JEDI_MIND_TRICK,
             LET_GO_OF_YOUR_HATRED,
             LUKES_IN_TROUBLE,
             MEDITATION,
             NEVER_TELL_ME_THE_ODDS,
             PROTECTION,
             ROYAL_COMMAND,
             THERMAL_DETONATOR,
             THROW_DEBRIS,
             WHIRLWIND,
             WISDOM,
             WOOKIE_HEALING,
             WOOKIE_INSTINCTS,
             WRATH_ANAKIN,
             WRATH_VADER,
             WRIST_CABLE,
             YOU_WILL_DIE,
             YOUR_SKILLS_ARE_NOT_COMPLETE,
             SUDDEN_ARRIVAL,
             BATTLEMIND,
             LATENT_FORCE_ABILITY,
             ANGER,
             ALL_TO_EASY,
             ASSASSINATION,
             ATHLETIC_SURGE,
             BOWCASTER,
             DARK_SIDE_DRAIN,
             DEADLY_AIM,
             DESPERATE_SHOT,
             FORCE_CONTROL,
             FORCE_STRIKE,
             GAMBLERS_LUCK,
             HEROIC_RETREAT,
             JEDI_ATTACK,
             JUSTICE,
             KYBER_DART,
             MASTERFUL_FIGHTING,
             PRECISE_SHOT,
             TAUNTING,
             ROCKET_RETREAT,
             SHOT_ON_THE_RUN,
             SITH_SPEED,
             SNIPER_SHOT,
             SUPER_SITH_SPEED,
             BLINDING_SURGE,
             COUNTER_ATTACK,
             FORCE_REBOUND,
             MARTIAL_DEFENSE,
             SERENITY,
             JEDI_BLOCK]
