"""Game states"""

from enum import Enum


class Board(Enum):
    """Board Types"""
    geonosis = 0
    freezing_chamber = 1
    kamino = 2
    throne_room = 3


class Card(Enum):
    """Card Types"""
    combat = 0
    special = 1
    power_attack = 2
    power_defense = 3
    power_combat = 4


class Square(Enum):
    """Base Square States"""
    dark = 0
    light = 1
    empty = 2
    obstacle = 3
    hole = 4


class Character(Enum):
    """Characters"""
    anakin_skywalker = 0
    han_solo = 1
    luke_skywalker = 2
    mace_windu = 3
    obi_wan = 4
    yoda = 5

    boba_fett = 6
    count_dooku = 7
    darth_maul = 8
    darth_vader = 9
    emperor_palpatine = 10
    jango_fett = 11

    chewbacca = 12
    greedo = 13
    leia_skywalker = 14
    padme_amidala = 15
    zam_wesell = 16

    crimson_guard = 17
    trooper = 18
    super_battle_droid = 19
    battle_droid = 20
