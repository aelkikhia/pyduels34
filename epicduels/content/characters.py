
from epicduels.content.game_state import Character as Ch
from epicduels.content.game_state import Square
from epicduels.content import decks

# Light is_main Characters
ANAKIN_SKYWALKER = {'name': 'Anakin Skywalker',
                    'hp': 18,
                    'is_main': True,
                    'max_hp': 18,
                    'type': Ch.anakin_skywalker,
                    'state': Square.light,
                    'pos': None,
                    'is_range': False,
                    'deck': decks.ANAKIN_SKYWALKER_DECK}

HAN_SOLO = {'name': 'Han Solo',
            'hp': 13,
            'is_main': True,
            'max_hp': 13,
            'type': Ch.han_solo,
            'state': Square.light,
            'pos': None,
            'is_range': True,
            'deck': decks.HAN_SOLO_DECK}

LUKE_SKYWALKER = {'name': 'Luke Skywalker',
                  'hp': 17, 'is_main': True,
                  'max_hp': 17,
                  'type': Ch.luke_skywalker,
                  'state': Square.light,
                  'pos': None,
                  'is_range': False,
                  'deck': decks.LUKE_SKYWALKER_DECK}

MACE_WINDU = {'name': 'Mace Windu',
              'hp': 19,
              'is_main': True,
              'max_hp': 19,
              'type': Ch.mace_windu,
              'state': Square.light,
              'pos': None,
              'is_range': False,
              'deck': decks.MACE_WINDU_DECK}

OBI_WAN = {'name': 'Obi-Wan Kenobi',
           'hp': 18,
           'is_main': True,
           'max_hp': 18,
           'type': Ch.obi_wan,
           'state': Square.light,
           'pos': None,
           'is_range': False,
           'deck': decks.OBI_WAN_KENOBI_DECK}

YODA = {'name': 'Yoda',
        'hp': 15,
        'is_main': True,
        'max_hp': 15,
        'type': Ch.yoda,
        'state': Square.light,
        'pos': None,
        'is_range': False,
        'deck': decks.YODA_DECK}

# minor Characters
CHEWBACCA = {'name': 'Chewbacca',
             'hp': 15,
             'is_main': False,
             'max_hp': 15,
             'type': Ch.chewbacca,
             'state': Square.light,
             'pos': None,
             'is_range': True,
             'deck': decks.CHEWBACCA_DECK}

LEIA_SKYWALKER = {'name': 'Leia Skywalker',
                  'hp': 10,
                  'is_main': False,
                  'max_hp': 10,
                  'type': Ch.leia_skywalker,
                  'state': Square.light,
                  'pos': None,
                  'is_range': True,
                  'deck': decks.LEIA_SKYWALKER_DECK}

PADME_AMIDALA = {'name': 'Padme Amidala',
                 'hp': 10,
                 'is_main': False,
                 'max_hp': 10,
                 'type': Ch.padme_amidala,
                 'state': Square.light,
                 'pos': None,
                 'is_range': True,
                 'deck': decks.PADME_AMIDALA_DECK}

TROOPER = {'name': 'Trooper',
           'hp': 4,
           'is_main': False,
           'max_hp': 4,
           'type': Ch.trooper,
           'state': Square.light,
           'pos': None,
           'is_range': True,
           'deck': decks.TROOPER_DECK}

# Dark is_main Characters
BOBA_FETT = {'name': 'Boba Fett',
             'hp': 14,
             'is_main': True,
             'max_hp': 14,
             'type': Ch.boba_fett,
             'state': Ch.dark,
             'pos': None,
             'is_range': True,
             'deck': decks.BOBA_FETT_DECK}

COUNT_DOOKU = {'name': 'Count Dooku',
               'hp': 18,
               'is_main': True,
               'max_hp': 18,
               'type': Ch.count_dooku,
               'state': Ch.dark,
               'pos': None,
               'is_range': False,
               'deck': decks.COUNT_DOOKU_DECK}

DARTH_MAUL = {'name': 'Darth Maul',
              'hp': 18,
              'is_main': True,
              'max_hp': 18,
              'type': Ch.darth_maul,
              'state': Ch.dark,
              'pos': None,
              'is_range': False,
              'deck': decks.DARTH_MAUL_DECK}

DARTH_VADER = {'name': 'Darth Vader',
               'hp': 20,
               'is_main': True,
               'max_hp': 20,
               'type': Ch.darth_vader,
               'state': Ch.dark,
               'pos': None,
               'is_range': False,
               'deck': decks.DARTH_VADER_DECK}

EMPEROR_PALPATINE = {'name': 'Emperor Palpatine',
                     'hp': 13,
                     'is_main': True,
                     'max_hp': 13,
                     'type': Ch.emperor_palpatine,
                     'state': Ch.dark,
                     'pos': None,
                     'is_range': False,
                     'deck': decks.EMPEROR_PALPATINE_DECK}

JANGO_FETT = {'name': 'Jango Fett',
              'hp': 15,
              'is_main': True,
              'max_hp': 15,
              'type': Ch.jango_fett,
              'state': Ch.dark,
              'pos': None,
              'is_range': True,
              'deck': decks.JANGO_FETT_DECK}

# minor Characters
GREEDO = {'name': 'Greedo',
          'hp': 7,
          'is_main': False,
          'max_hp': 7,
          'type': Ch.greedo,
          'state': Ch.dark,
          'pos': None,
          'is_range': True,
          'deck': decks.GREEDO_DECK}

ZAM_WESELL = {'name': 'Zam Wesell',
              'hp': 10,
              'is_main': False,
              'max_hp': 10,
              'type': Ch.zam_wesell,
              'state': Ch.dark,
              'pos': None,
              'is_range': True,
              'deck': decks.ZAM_WESELL_DECK}

BATTLE_DROID = {'name': 'Battle Droid',
                'hp': 3,
                'is_main': False,
                'max_hp': 3,
                'type': Ch.battle_droid,
                'state': Ch.dark,
                'pos': None,
                'is_range': True,
                'deck': decks.TROOPER_DECK}

CRIMSON_GUARD = {'name': 'Crimson Guard',
                 'hp': 5,
                 'is_main': False,
                 'max_hp': 5,
                 'type': Ch.crimson_guard,
                 'state': Ch.dark,
                 'pos': None,
                 'is_range': True,
                 'deck': decks.CRIMSON_GUARD_DECK}

SUPER_BATTLE_DROID = {'name': 'Super Battle Droid',
                      'hp': 5,
                      'is_main': False,
                      'max_hp': 5,
                      'type': Ch.super_battle_droid,
                      'state': Ch.dark,
                      'pos': None,
                      'is_range': True,
                      'deck': decks.SUPER_BATTLE_DROID_DECK}

CHARACTERS = {
    Ch.anakin_skywalker: ANAKIN_SKYWALKER,
    Ch.han_solo: HAN_SOLO,
    Ch.luke_skywalker: LUKE_SKYWALKER,
    Ch.mace_windu: MACE_WINDU,
    Ch.obi_wan: OBI_WAN,
    Ch.yoda: YODA,
    Ch.boba_fett: BOBA_FETT,
    Ch.count_dooku: COUNT_DOOKU,
    Ch.darth_maul: DARTH_MAUL,
    Ch.darth_vader: DARTH_VADER,
    Ch.emperor_palpatine: EMPEROR_PALPATINE,
    Ch.jango_fett: JANGO_FETT,
    Ch.chewbacca: CHEWBACCA,
    Ch.trooper: TROOPER,
    Ch.leia_skywalker: LEIA_SKYWALKER,
    Ch.padme_amidala: PADME_AMIDALA,
    Ch.battle_droid: BATTLE_DROID,
    Ch.crimson_guard: CRIMSON_GUARD,
    Ch.greedo: GREEDO,
    Ch.super_battle_droid: SUPER_BATTLE_DROID,
    Ch.zam_wesell: ZAM_WESELL
}

SQUADS = {
    Ch.anakin_skywalker: [ANAKIN_SKYWALKER, PADME_AMIDALA],
    Ch.han_solo: [HAN_SOLO, CHEWBACCA],
    Ch.luke_skywalker: [LUKE_SKYWALKER, LEIA_SKYWALKER],
    Ch.mace_windu: [MACE_WINDU, TROOPER, TROOPER],
    Ch.obi_wan: [OBI_WAN, TROOPER, TROOPER],
    Ch.yoda: [YODA, TROOPER, TROOPER],
    Ch.boba_fett: [BOBA_FETT, GREEDO],
    Ch.count_dooku: [COUNT_DOOKU, SUPER_BATTLE_DROID, SUPER_BATTLE_DROID],
    Ch.darth_maul: [DARTH_MAUL, BATTLE_DROID, BATTLE_DROID],
    Ch.darth_vader: [DARTH_VADER, TROOPER, TROOPER],
    Ch.emperor_palpatine: [EMPEROR_PALPATINE, CRIMSON_GUARD, CRIMSON_GUARD],
    Ch.jango_fett: [JANGO_FETT, ZAM_WESELL]
}
